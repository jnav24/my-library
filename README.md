What is this?
-------------

This is my personal mixin library to use as a base for all new web projects. I also added some of my most used functions into this file also. Everything is in alphabetical order and grouped by type.

CSS3PIE is also needed for certain values like the linear-gradient to use the -pie- syntax rather than filter because the filter syntax is buggy on IE (of course!). To get CSS3PIE, go [here](http://css3pie.com/).